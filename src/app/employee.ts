export interface Employee {
    id: number;
    fname: string;
    lname: string;
    dept:string;
    city:string;
    email:string;
  }