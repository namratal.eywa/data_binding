import { isNgTemplate } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { DefaultColorOnEventDirective } from '../directive/higlite.directive';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employee: Employee = {
    id: 1,
    fname: 'Namrata',
    lname: 'lagshetti',
    dept: 'cherry pick',
    city: 'solapur',
    email: 'abc@gmail.com'
  };
  item :any;
  errorItem : boolean = false;
  
  constructor() { }

  ngOnInit(): void {
   }
   sample: string[] = [];
  tempArray = [
    {
      "id": 43,
      "fname":"namrta",
      "lname":"lagshetti",
      "dept":"sales",
      "city":"solapur",
      "email":"abc@gmail.com"
    },
    {
      "id": 44,
      "fname":"namrta",
      "lname":"lagshetti",
      "dept":"sales",
      "city":"solapur",
      "email":"pdc@gmail.com"
    },
    {
      "id": 45,
      "fname":"namrta",
      "lname":"lagshetti",
      "dept":"sales",
      "city":"solapur",
      "email":"xyzc@gmail.com"
    }
  ];

  editemp(item:any){
    if(this.sample.indexOf(item) === -1){
      this.sample.push(item);
      this.errorItem = false;
      console.log(item);
    }
    else if (this.sample.indexOf(item)> -1){
      this.errorItem = true;
      console.log(item);
    }
  }
}
