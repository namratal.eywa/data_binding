import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appBgcolor]'
})
export class BgcolorDirective {

  constructor(private elRef: ElementRef) { }

  @HostListener('mouseover')onMouseEnter() {
    //this.changeColor('#0000ff');
    this.elRef.nativeElement.style.backgroundColor = 'red';
  }
  @HostListener('mouseleave')onMouseLeave() {
    this.changeColor('');
  }
  private changeColor(color: string) {
    this.elRef.nativeElement.style.backgroundColor = color;
  }


}
