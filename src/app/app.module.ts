import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { DefaultColorOnEventDirective } from './directive/higlite.directive';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { BgcolorDirective } from './bgcolor.directive';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    DefaultColorOnEventDirective,
    BgcolorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
